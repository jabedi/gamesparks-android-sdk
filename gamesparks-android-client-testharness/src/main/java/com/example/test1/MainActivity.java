package com.example.test1;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.gamesparks.sdk.GS;
import com.gamesparks.sdk.GSEventConsumer;
import com.gamesparks.sdk.android.*;
import com.gamesparks.sdk.api.GSData;
import com.gamesparks.sdk.api.autogen.GSMessageHandler;
import com.gamesparks.sdk.api.autogen.GSMessageHandler.*;
import com.gamesparks.sdk.api.autogen.GSTypes.LeaderboardData;
import com.gamesparks.sdk.api.autogen.GSResponseBuilder;
import com.gamesparks.sdk.api.autogen.GSResponseBuilder.*;

public class MainActivity extends ActionBarActivity
{	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
	
		setContentView(R.layout.activity_main);
		
		GSAndroidPlatform.initialise(this, "296486aA0cls", "09sqYnhkEtAqCtzYKGwELFN4cN6xtgCG", false, true);
	
		GSAndroidPlatform.gs().getMessageHandler().setScriptMessageListener(new GSEventConsumer<GSMessageHandler.ScriptMessage>() {
			@Override
			public void onEvent(ScriptMessage event) {
				System.out.println(Thread.currentThread().getId() + " WE GOT A MESSAGE: " + event.getExtCode());
			}
		});

		GSAndroidPlatform.gs().setOnAvailable(new GSEventConsumer<Boolean>() {
			@Override
			public void onEvent(Boolean available) {
				TextView helloWorld = (TextView) findViewById(R.id.hello_world);
				helloWorld.setText(Thread.currentThread().getId() + " AVAILABLE: " + available);

				if (available) {
					sendAuth();
				}
			}
		});
		
		GSAndroidPlatform.gs().setOnAuthenticated(new GSEventConsumer<String>()
		{
			@Override
		    public void onEvent(String playerId)
		    {
				System.out.println(Thread.currentThread().getId() + " PLAYER ID: "+ playerId);
		    }
		});

		Map<String,Object> scriptData = new HashMap<>();
		//Populate data
		scriptData.put("name", "bla");
		scriptData.put("age", "1");
		scriptData.put("gender", "M");

		/*	@Override
			public void onEvent(GSResponseBuilder.LogEventResponse logEventResponse) {

			}
		});*/


				GSAndroidPlatform.gs().getRequestBuilder().createLogEventRequest().setEventKey("setDetails").setJSONEventAttribute("scriptData", scriptData).send(new GSEventConsumer<GSResponseBuilder.LogEventResponse>() {
					@Override
					public void onEvent(GSResponseBuilder.LogEventResponse logEventResponse) {

					}
				});
	}
	
	@Override
	public void onStart()
	{
		super.onStart();
		
		TextView helloWorld = (TextView)findViewById(R.id.editText1);
		helloWorld.setText("MainActivity : " + Thread.currentThread().getId());
		
		GSAndroidPlatform.gs().start();
	}
	
	@Override
	public void onStop()
	{
		super.onStop();
		
		GSAndroidPlatform.gs().stop();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		
		if (id == R.id.action_settings)
		{
			return true;
		}
		
		return super.onOptionsItemSelected(item);
	}
	
	
	@Override
	public void onResume()
	{
	    super.onResume();
	}
	
	@Override
	public void onPause()
	{
	    super.onPause();
	}
	
	private void sendAuth()
	{
		GSAndroidPlatform.gs().getRequestBuilder().createAuthenticationRequest()
				.setPassword("password").setUserName("userName")
				.send(new GSEventConsumer<AuthenticationResponse>()
				{
					@Override
					public void onEvent(AuthenticationResponse response)
					{
						System.out.println(Thread.currentThread().getId() + " WE GOT AN AUTH RESPONSE : " + response.getDisplayName());
						
						sendLbRequest();
					}
				});
	}

	private void sendLbRequest()
	{
		GSAndroidPlatform.gs().getRequestBuilder().createAroundMeLeaderboardRequest()
				.send(new GSEventConsumer<AroundMeLeaderboardResponse>()
				{
					@Override
					public void onEvent(AroundMeLeaderboardResponse response)
					{
						System.out.println(Thread.currentThread().getId() + " WE GOT A RESPONSE " + response.toString());

						if (response.getScriptData() != null)
						{
							Long myNumber = (Long) response.getScriptData()
									.getAttribute("myNumber");
							String myString = (String) response.getScriptData()
									.getAttribute("myString");
						}

						String challengeInstanceId = response
								.getChallengeInstanceId();
						List<LeaderboardData> data = response.getData();
						List<LeaderboardData> first = response.getFirst();
						List<LeaderboardData> last = response.getLast();
						String leaderboardShortCode = response
								.getLeaderboardShortCode();
						GSData scriptData = response.getScriptData();
						Boolean social = response.getSocial();
					}
				});
	}

}
